# EnieCorrector

## Description: 
 A simple Corrector for writing in spanish, so instead of using the character "ñ" just use "ni" instead, then run this app and it will replace 'ni' for 'ñ'", its important to notice that fot this to work words like arañita you should write as araniita to work properly.

## Install

```bash
npm i eniecorrector
```

## Use

```bash
enie inputfile outpufile
```

If no outfile is passed it will assume that the ouput file is the same as the inputfile

