#!/usr/bin/env node

// Imports
'use-strict';

let correctFile = require('../Functions/replacer.js');

var url = process.argv.slice(2);

if (url.length === 0) {
	console.log('Error: You must pass a file as an argument');
	process.exit(1);
}

correctFile.CorrectFile(url);
