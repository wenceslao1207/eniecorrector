#!/usr/bin/env 	node

// Imports
let fs = require('fs');
let exceptions = require('../Dictionary/dictionary.js');

const vocals = ['a', 'e', 'i', 'o', 'u'];

var replacer = function( word ) {
	var exceptionList = exceptions.exceptionList;

	if (exceptionList.indexOf(word) != -1) return word;

	var position = word.indexOf("ni");

	if (position === -1) return word

	if (word.slice(position +2).indexOf("ni") != -1) {
		positionSecond = word.slice(position +2).indexOf('ni') + 2 ;
		for (var i=0; i < vocals.length; i++) {
			if (word[positionSecond+2] === vocals[i]) {
				temp = word.substring(positionSecond, positionSecond + 2);
				first = word.slice(position,position+2);
				word = word.slice(position + 2);
				word = word.replace(temp, 'ñ');
				return first + word
			}
		}
	}

	for (var i=0; i < vocals.length; i++) {
		if (word[position+2] === vocals[i]) {
			temp = word.substring(position, position + 2);
			word = word.replace(temp, 'ñ');
			return word;
		}
	}
	return word

}

CorrectFile = function(url){
	fs.readFile(url[0], 'utf8', function(err, data){
		var output = '';
		if (err) throw err;
		var lines = data.toString().split('\n')
		for (var i=0; i < lines.length; i++) {
			words = lines[i].split(' ');
			lines[i] = '';
			for (var j=0; j < words.length; j++){
				words[j] = replacer(words[j]);
				lines[i] += words[j] + ' ';
			}
			output += lines[i] + '\n';
		}

		urlOuput = ((url.length === 1) ? url[0] : url[1])
		fs.writeFile (urlOuput, output, function(err) {
        	if (err) throw err;
        	console.log('complete');
        });
	});
	console.log(url);
}

exports.CorrectFile = CorrectFile;
